﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cardea.RevelReader
{
    public enum StatusCode
    {
        FAILURE = -1, SUCCESS = 0, UNKNOWN = 1
    }
}
