﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using System.Timers;

/*
User opens reader software and enters run information
User ‘starts’ protocol in reader software
Reader software writes ‘ready’  0 to ReaderStatus.csv, saves and closes (R2N)
Reader software monitors ReaderCmd.csv for ‘continue’ 1 signal (N2R)

Nimbus pipettes reagent:

Reader software detects the continue command - begins calibration step on indicated chips.
Reader software overwrites ‘continue’ 1 with ‘wait’ 0 in ReaderCmd.csv
Reader software executes calibration step – when complete Reader software overwrites ‘not ready’ 0 with ‘ready’ 1 in ReaderStatus.csv
Reader software monitors ReaderCmd for ‘continue’
*/

namespace Cardea.RevelReader
{


    /// <summary>
    /// This is the working horse of the whole program, that will interact with Niubus and do the read operations and emit/raise expected events
    /// </summary>
    /// 
    
     //TODO: I feel we need let this class implements ICardeaRevelReaderEventsInterface, but for now
    // let's wait for the inputs from Hamilton side

    public class CardeaRevelReader : ICardeaRevelReader //, ICardeaRevelReaderEventsInterface
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(CardeaRevelReader));

        private static readonly string N2R_READER_COMMAND_CSV_FILE = ConfigurationManager.AppSettings["N2R_READER_COMMAND_CSV_FILE"];
        private static readonly string R2N_READER_STATUS_CSV_FILE = ConfigurationManager.AppSettings["N2R_READER_COMMAND_CSV_FILE"];

        private static string POLLING_TIMER_DURATION_IN_MS = ConfigurationManager.AppSettings["POLLING_TIMER_DURATION_IN_MS"];

        private static readonly string GUID_STR = "1b430f55-eb49-4461-a6a7-cb523deb6cbb";

        private Timer _timer;

        private DateTime _dtWritenToReaderStatusCsv = DateTime.MinValue;
        private DateTime _dtReadFromReaderCmdCsv = DateTime.MinValue;

        private StringBuilder _sb = new StringBuilder();

        private long _progId;
        private Guid _guid;

        
        public CardeaRevelReader()
        {
            log4net.Config.XmlConfigurator.Configure();

            this._progId = 333666999;

            this._guid = new Guid(GUID_STR);

            int interval = 10000;
            if (!string.IsNullOrEmpty(POLLING_TIMER_DURATION_IN_MS))
            {
                interval = int.Parse(POLLING_TIMER_DURATION_IN_MS);
            }

            _timer = new Timer
            {
                Interval = interval
            };
            _timer.Elapsed += new ElapsedEventHandler(this.OnPollingTimer);
            _timer.Start();
            

            Initialize();
            log.Info("Cardea Revel initialized, and the polling timer started ...");
        }

        public long ProgId
        {
            get
            {
                return this.ProgId;
            }

        }

        public Guid Guid
        {
            get
            {
                return this._guid;
            }
        }



        private void ResetFileContent()
        {
            try
            {
                using (var sw = new StreamWriter(R2N_READER_STATUS_CSV_FILE))
                {
                    sw.WriteLine("");
                }

                using (var sw = new StreamWriter(N2R_READER_COMMAND_CSV_FILE))
                {
                    sw.WriteLine("");
                }

            }
            catch(Exception ex)
            {
                log.Error("Exception encountered in ResetFileContent(), ex=" + ex);
            }

        }

        private int GetContentFromReaderCmdCsv()
        {
            int ret = -1;
            try
            {
                using (var sr = new StreamReader(N2R_READER_COMMAND_CSV_FILE))
                {
                    string val = sr.ReadToEnd().Trim();
                    if (string.IsNullOrEmpty(val)) return -1;
                    log.Info("Retrieved content from " + N2R_READER_COMMAND_CSV_FILE + ", val=" + val);
                    ret = int.Parse(val);
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception encountered in GetContentFromReaderCmdCsv(), ex=" + ex);
            }

            return ret;
        }


        private void OnPollingTimer(object sender, ElapsedEventArgs args)
        {
            try
            {
                int readerCmd = GetContentFromReaderCmdCsv();
                if (readerCmd == 1)
                {
                    if(this._dtReadFromReaderCmdCsv != DateTime.MinValue 
                        && this._dtWritenToReaderStatusCsv != DateTime.MinValue
                        && this._dtReadFromReaderCmdCsv <= this._dtWritenToReaderStatusCsv)
                    {
                        log.Info("Alive and job done, doing nothing");
                        return;
                    }

                    _dtReadFromReaderCmdCsv = DateTime.Now;
                    log.Info("Notified by Nimbus to continue");
                    Continue();
                } else
                {
                    log.Info("Alive, but Nimbus is not ready");
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception encountered in OnPollingTimer, ex=" + ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private StatusCode ReadAllData()
        {
            try
            {
                log.Info("Begin reading data at time=" + System.DateTime.Now);


                log.Info("End reading data at time=" + System.DateTime.Now);
            }
            catch (Exception ex)
            {
                log.Error("Exception encountered in ReadAllData(), ex=" + ex);
                return StatusCode.FAILURE;
            }
            return StatusCode.SUCCESS;
        }


        /// <summary>
        /// 
        /// </summary>
        private StatusCode DoChipCalibration()
        {
            try
            {
                log.Info("Begin doing chip calibration at time=" + System.DateTime.Now);


                log.Info("End doing chip calibration at time=" + System.DateTime.Now);
            }
            catch (Exception ex)
            {
                log.Error("Exception encountered in DoChipCalibration(), ex=" + ex);
            }
            return StatusCode.SUCCESS;
        }

        /// <summary>
        /// Reader software writes ‘ready’  0 to ReaderStatus.csv, saves and closes (R2N)
        /// </summary>
        private StatusCode WriteReadyToReaderStatusCsv()
        {
            try
            {
                using (var sw = new StreamWriter(R2N_READER_STATUS_CSV_FILE))
                {
                    sw.WriteLine("0");
                    this._dtWritenToReaderStatusCsv = DateTime.Now;
                }
            } catch(Exception ex)
            {
                log.Error("Exception encountered in WriteReadyToReaderStatusCsv(), ex=" + ex);
                return StatusCode.FAILURE;
            }
            return StatusCode.SUCCESS;
        }

        public StatusCode Continue()
        {
            try
            {
                DoChipCalibration();
                WriteReadyToReaderStatusCsv();
            }
            catch (Exception ex)
            {
                log.Error("Exception encountered in Continue(), ex=" + ex);
                return StatusCode.FAILURE;
            }
            return StatusCode.SUCCESS;
        }


        public StatusCode Initialize()
        {
            try
            {
                ResetFileContent();
                Start();
            }
            catch (Exception ex)
            {
                log.Error("Exception encountered in Initialize(), ex=" + ex);
                return StatusCode.FAILURE;
            }
            return StatusCode.SUCCESS;
        }

        public StatusCode Start()
        {
            try
            {
                WriteReadyToReaderStatusCsv();
            } catch(Exception ex)
            {
                log.Error("Exception encountered in Start(), ex=" + ex);
                return StatusCode.FAILURE;
            }
            return StatusCode.SUCCESS;
        }

        public string GetLastError()
        {
            return this._sb.ToString();
        }

        public string GetErrorString(int code)
        {
            if(code < 0)
            {
                return "failure";
            }
            else if (code == 0)
            {
                return "success";

            }
            else
            {
                return "unknown error";
            }
        }


        #region unused
        public StatusCode LoadMethod(int index)
        {
            try
            {
                throw new NotImplementedException("this method not implemented yet");
            }
            catch (Exception ex)
            {
                log.Error("Exception encountered in Stop(), ex=" + ex);
                return StatusCode.FAILURE;
            }
            //return StatusCode.SUCCESS;
        }


        public StatusCode Stop(double duration)
        {
            try
            {
                throw new NotImplementedException("this method not implemented yet");
            }
            catch (Exception ex)
            {
                log.Error("Exception encountered in Stop(), ex=" + ex);
                return StatusCode.FAILURE;
            }
            //return StatusCode.SUCCESS;
        }

        public StatusCode Halt()
        {
            try
            {
                throw new NotImplementedException("this method not implemented yet");
            }
            catch (Exception ex)
            {
                log.Error("Exception encountered in Halt(), ex=" + ex);
                return StatusCode.FAILURE;
            }
            //return StatusCode.SUCCESS;
        }

        #endregion unused
    }
}
