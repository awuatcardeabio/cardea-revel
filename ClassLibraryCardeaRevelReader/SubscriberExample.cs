﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cardea.RevelReader
{
    class SubscriberExample
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(SubscriberExample));

        private ICardeaRevelReaderEventsInterface cardeaRevelReaderEventSource;

        public SubscriberExample(ICardeaRevelReaderEventsInterface cardeaRevelReaderEventSource)
        {
            this.cardeaRevelReaderEventSource = cardeaRevelReaderEventSource;
            this.cardeaRevelReaderEventSource.DataReadyForRead += this.OnReadyToRead;

        }

        void OnReadyToRead(object sender, EventArgs e)
        {
            log.Info("Entering OnReadyToRead()");



            log.Info("Exiting OnReadyToRead()");
        }

        void OnReadAllDataCompleted(object sender, EventArgs e)
        {
            log.Info("Entering OnReadAllDataCompleted()");


            log.Info("Exiting OnReadAllDataCompleted()");
        }
    }
}
