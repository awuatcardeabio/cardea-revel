﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;


namespace Cardea.RevelReader
{
    [ComSourceInterfaces("ICardeaRevelReader.ICardeaRevelReaderEventsInterface")]
    [GuidAttribute("1b430f55-eb49-4461-a6a7-cb523deb6cbb")]
    public class CardeaRevelReaderEventSource : ICardeaRevelReaderEventsInterface
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(CardeaRevelReaderEventSource));

        private event EventHandler AllDataReadCompletedEvent;
        private event EventHandler DataReadyForReadEvent;

        private object objectLock = new Object();
        event EventHandler ICardeaRevelReaderEventsInterface.AllDataReadCompleted
        {
            add
            {
                lock (objectLock)
                {
                    AllDataReadCompletedEvent += value;
                }
            }

            remove
            {
                lock (objectLock)
                {
                    AllDataReadCompletedEvent -= value;
                }
            }
        }

        event EventHandler ICardeaRevelReaderEventsInterface.DataReadyForRead
        {
            add
            {
                lock (objectLock)
                {
                    DataReadyForReadEvent += value;
                }
            }

            remove
            {
                lock (objectLock)
                {
                    DataReadyForReadEvent -= value;
                }
            }
        }


        public void DoReadAllData()
        {
            log.Info("To signal the ready for read event");
            this.DataReadyForReadEvent?.Invoke(this, EventArgs.Empty);

            log.Debug("Do the read all operation");


            log.Info("To signal the all data read complete event");
            this.AllDataReadCompletedEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}
