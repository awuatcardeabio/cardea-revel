﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;

namespace Cardea.RevelReader
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ICardeaRevelReaderEventsInterface eventSource = new CardeaRevelReaderEventSource();
            SubscriberExample subscriberExample = new SubscriberExample(eventSource);

            Console.ReadLine();
        }
    }
}
