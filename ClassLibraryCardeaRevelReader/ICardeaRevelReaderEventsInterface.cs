﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Cardea.RevelReader
{
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    //TODO: do we need add GuidAttribute here
    //if so, then are we going to use the same GUID as above
    //if different GUID is used, then we will end up with two distinct GUIDs
    //currently we only expose the above GUID
    public interface ICardeaRevelReaderEventsInterface
    {
        event EventHandler AllDataReadCompleted;

        event EventHandler DataReadyForRead;
    }
}
