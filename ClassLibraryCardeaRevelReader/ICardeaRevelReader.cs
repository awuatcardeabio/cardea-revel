﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Cardea.RevelReader
{
    [GuidAttribute("1b430f55-eb49-4461-a6a7-cb523deb6cbb")]
    interface ICardeaRevelReader
    {
        StatusCode Initialize();
        StatusCode Start();
        StatusCode Stop(double duration);
        StatusCode Halt();
        StatusCode Continue();
        StatusCode LoadMethod(int index);

        string GetLastError();
        string GetErrorString(int code);

        //TODO: to define ping and echo here?
        //StatusCode Ping();
        //StatusCode Echo();
    }
   
}
